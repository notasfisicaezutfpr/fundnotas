Conceitos Básicos
=================

*Leitura Recomendada: Halliday, Resnick & Walker: Fundamentos de Física, vol. 1 - Cap. 1 - Medição*
:math:`\phantom{\text{pargraph}}` :math:`\phantom{\text{pargraph}}` *Moysés Nussenzveig, Física Básica, vol. 1, Cap. 1 -*


:math:`\phantom{\text{pargraph}}`  A Física teve e continua tendo um papel fundamental no desenvolvimento da sociedade, seja na explicação de fenômenos físicos conhecidos, da descoberta de novos fenômenos ou no desenvolvimento de novas tecnologias. Isso se deve ao fato da Física ser uma ciência baseada em experimentos. Estes permitem verificar a validade de uma dada teoria quando comparamos as previsões dos modelos teóricos com aqueles obtidos por meio dos experimentos. Deste modo, fica evidente que dados são a matéria-prima no estudo de qualquer fenômeno que estejamos interessados em conhecer ou estudar. Com efeito, precisamos saber como tratamos e apresentamos um determinado dado obtido de um experimento.
